---
title: Copertina
description: An easy guide to learn how to create mods for Minetest
layout: default
homepage: true
no_header: true
root: ..
idx: 0.1
---

<header>
    <h1>Minetest: Libro del Moddaggio</h1>

    <span>di <a href="https://rubenwardy.com" rel="author">rubenwardy</a></span>
    <span>con modifiche di <a href="http://rc.minetest.tv/">Shara</a></span>
    <span>traduzione italiana di <a href="https://liberapay.com/Zughy/">Zughy</a></span>
</header>

## Introduzione

Il moddaggio su Minetest è supportato grazie a script in Lua.
Questo libro mira a insegnarti come si crea una mod, iniziando dalle basi: ogni capitolo si concentra su un aspetto specifico dell'API, così da arrivare in breve tempo a farti creare i tuoi contenuti.

Oltre che [leggere questo libro su internet](https://rubenwardy.com/minetest_modding_book),
puoi anche [scaricarlo in HTML](https://github.com/rubenwardy/minetest_modding_book/releases).

### Riscontri e Contributi

Hai notato un errore o vuoi dirmi la tua? Assicurati di farmelo presente.

* Apri una [Segnalazione su GitLab](https://gitlab.com/rubenwardy/minetest_modding_book/-/issues).
* Rispondi alla [Discussione sul Forum](https://forum.minetest.net/viewtopic.php?f=14&t=10729).
* [Contattami (in inglese)](https://rubenwardy.com/contact/).
* Voglia di contribuire?
  [Leggi il README](https://gitlab.com/rubenwardy/minetest_modding_book/-/blob/master/README.md).
